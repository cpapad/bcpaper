/*
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"encoding/json"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

//Function to check if tenant is Registered
func readTenant(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Pck)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Pck) must be a non-empty string")
	}

	pck := args[0]
	tenantAsBytes, err := stub.GetState(pck)

	if err != nil {
		return shim.Error("Failed to get tenant:" + pck)
	}
	if tenantAsBytes == nil {
		jsonResp := "{\"Error\":\"Tenant does not exist: " + pck + "\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(tenantAsBytes)

}

//Function to register Tenant
func registerTenant(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	//Input sanitization
	if len(args) != 4 {
		return shim.Error("Incorect number of arguments. Excpecting 4")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument (Pck) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument (Name) must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument (Email) must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument (Phone) must be a non-empty string")
	}

	tenant := Tenant{
		ObjectType: "tenant",
		Pck:        args[0],
		Name:       args[1],
		Email:      args[2],
		Phone:      args[3],
	}
	tenantAsBytes, _ := json.Marshal(tenant)
	err := stub.PutState(tenant.Pck, tenantAsBytes)

	if err != nil {
		return shim.Error("Failed to create tenant " + tenant.Name)
	}

	return shim.Success(tenantAsBytes)

}

//Function to update Tenant
func updateTenant(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	//Input sanitization
	if len(args) != 4 {
		return shim.Error("Incorect number of arguments. Excpecting 4")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument (Pck) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument (Name) must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument (Email) must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument (Phone) must be a non-empty string")
	}

	tenantAsBytes, err := stub.GetState(args[0])

	//Check if tenant due to update exists
	if err != nil {
		return shim.Error("Failed to get tenant:" + err.Error())
	}
	if tenantAsBytes == nil {
		jsonResp := "{\"Error\":\"Tenant does not exist: " + args[0] + "\"}"
		return shim.Error(jsonResp)
	}

	tenant := new(Tenant)
	_ = json.Unmarshal(tenantAsBytes, tenant)

	tenant.Pck = args[0]
	tenant.Name = args[1]
	tenant.Email = args[2]
	tenant.Phone = args[3]

	tenantAsBytes, _ = json.Marshal(tenant)

	err = stub.PutState(tenant.Pck, tenantAsBytes)

	if err != nil {
		return shim.Error("Failed to create tenant " + tenant.Name)
	}

	return shim.Success(tenantAsBytes)

}

//Function to delete Tenant
func unregisterTenant(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	var jsonResp string

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Pck)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Pck) must be a non-empty string")
	}

	pck := args[0]
	tenantAsBytes, err := stub.GetState(pck)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + pck + "\"}"
		return shim.Error(jsonResp)
	} else if tenantAsBytes == nil {
		jsonResp = "{\"Error\":\"Tenant does not exist: " + pck + "\"}"
		return shim.Error(jsonResp)
	}

	err = stub.DelState(pck) //remove the flow from chaincode state
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}

	return shim.Success(tenantAsBytes)

}
