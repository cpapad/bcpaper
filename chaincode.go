/*
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Tenant data struct def
type Tenant struct {
	ObjectType string `json:"docType"`
	Pck        string `json:"pck"`
	Name       string `json:"name"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
}

//NetworkService data struct def
type NetworkService struct {
	ObjectType string `json:"docType"`
	ID         string `json:"id"`
	Name       string `json:"name"`
	Tenant     Tenant `json:"tenant"`
}

//Service data struct def
type Service struct {
	ObjectType  string `json:"docType"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	ShortName   string `json:"short_name"`
	Description string `json:"description"`
	NsdName     string `json:"nsd_name"`
	Provider    string `json:"provider"`
}

//Lease data struct def
type Lease struct {
	ObjectType string   `json:"docType"`
	Grantor    string   `json:"grantor"`
	Recipient  string   `json:"recipient"`
	Service    string   `json:"vnf"`
	Issue      int64    `json:"issue"`
	Expiry     int64    `json:"expiry"`
	Revokers   []string `json:"revokers"`
}

// Chaincode is the definition of the chaincode structure.
type Chaincode struct {
}

// Init is called when the chaincode is instantiated by the blockchain network.
func (cc *Chaincode) Init(stub shim.ChaincodeStubInterface) sc.Response {
	//fcn, params := stub.GetFunctionAndParameters()

	//Declare tenants
	cpapad := Tenant{ObjectType: "tenant", Pck: "1", Name: "Konstantinos", Email: "cpapad@netmode.ntua.gr", Phone: "6948564657"}
	ddechou := Tenant{ObjectType: "tenant", Pck: "2", Name: "Dimitris", Email: "ddechou@netmode.ntua.gr", Phone: "6973562522"}

	//Declare network services (slices) and their owners
	s1 := NetworkService{ObjectType: "network_service", ID: "slice_hackfest_1", Name: "slice_hackfest_1", Tenant: cpapad}
	s2 := NetworkService{ObjectType: "network_service", ID: "slice_hackfest_2", Name: "slice_hackfest_2", Tenant: ddechou}

	//Declare Services with providers (slices)
	service1 := Service{ObjectType: "service", ID: "cosmos_big_vnfd", Name: "cosmos_big_vnfd", ShortName: "cosmos_big_vnfd", Description: "Ubuntu1804 with Tensorflow", NsdName: "cosmos_big_vnfd", Provider: "slice_hackfest_1"}
	service2 := Service{ObjectType: "service", ID: "cosmos_small_vnfd", Name: "cosmos_small_vnfd", ShortName: "cosmos_small_vnfd", Description: "Ubuntu1604 with Tensorflow", NsdName: "cosmos_small_vnfd", Provider: "slice_hackfest_2"}

	//Declare delegations
	lease1 := Lease{ObjectType: "lease", Grantor: "slice_hackfest_1", Recipient: "slice_hackfest_2", Service: "cosmos_big_vnfd", Issue: 1590451200, Expiry: 1590969600, Revokers: []string{"1", "2"}}
	lease2 := Lease{ObjectType: "lease", Grantor: "slice_hackfest_2", Recipient: "slice_hackfest_1", Service: "cosmos_small_vnfd", Issue: 1588291200, Expiry: 1589932800, Revokers: []string{"1", "2"}}

	//convert tenant to []byte
	cpapadAsJSONBytes, _ := json.Marshal(cpapad)
	//add cpapad to ledger
	err := stub.PutState(cpapad.Pck, cpapadAsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create tenant " + cpapad.Name)
	}

	//convert tenant to []byte
	ddechouAsJSONBytes, _ := json.Marshal(ddechou)
	//add ddechou to ledger
	err = stub.PutState(ddechou.Pck, ddechouAsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create tenant " + ddechou.Name)
	}

	//Add s1 to ledger
	s1AsJSONBytes, _ := json.Marshal(s1)
	err = stub.PutState(s1.ID, s1AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create network service " + s1.Name)
	}

	//Add s2 to ledger
	s2AsJSONBytes, _ := json.Marshal(s2)
	err = stub.PutState(s2.ID, s2AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create network service " + s2.Name)
	}

	//Add leases to ledger
	lease1AsJSONBytes, _ := json.Marshal(lease1)
	err = stub.PutState(lease1.Service+","+lease1.Recipient, lease1AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create lease " + lease1.Service + "," + lease1.Recipient)
	}

	lease2AsJSONBytes, _ := json.Marshal(lease2)
	err = stub.PutState(lease2.Service+","+lease2.Recipient, lease2AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create lease " + lease2.Service + "," + lease2.Recipient)
	}

	//Add NFVs to ledger
	service1AsJSONBytes, _ := json.Marshal(service1)
	err = stub.PutState(service1.ID, service1AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create Service " + service1.Name)
	}

	service2AsJSONBytes, _ := json.Marshal(service2)
	err = stub.PutState(service2.ID, service2AsJSONBytes)
	if err != nil {
		return shim.Error("Failed to create Service " + service2.Name)
	}

	//fmt.Println("Init()", fcn, params)
	return shim.Success(cpapadAsJSONBytes)
}

// Invoke is called as a result of an application request to run the chaincode.
func (cc *Chaincode) Invoke(stub shim.ChaincodeStubInterface) sc.Response {
	fcn, params := stub.GetFunctionAndParameters()

	fmt.Println("Invoke()", fcn, params)
	fmt.Println("Function:", fcn)

	if fcn == "readTenant" {
		return readTenant(stub, params)
	} else if fcn == "registerTenant" {
		return registerTenant(stub, params)
	} else if fcn == "deleteTenant" {
		return unregisterTenant(stub, params)
	} else if fcn == "updateTenant" {
		return updateTenant(stub, params)
	} else if fcn == "readNService" {
		return readNService(stub, params)
	} else if fcn == "registerNService" {
		return registerNService(stub, params)
	} else if fcn == "unregisterNService" {
		return unregisterNService(stub, params)
	} else if fcn == "readLease" {
		return readLease(stub, params)
	} else if fcn == "registerLease" {
		return registerLease(stub, params)
	} else if fcn == "isLeaseValid" {
		return isLeaseValid(stub, params)
	} else if fcn == "readService" {
		return readService(stub, params)
	} else if fcn == "registerService" {
		return registerService(stub, params)
	} else if fcn == "unregisterService" {
		return unregisterService(stub, params)
	}

	return shim.Error("Received unknown function invocation")
}
