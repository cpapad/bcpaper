/*
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"encoding/json"
	//"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

//Function to read Network Service
func readNService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Network Service Id)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Network Service Id) must be a non-empty string")
	}

	id := args[0]
	serviceAsBytes, err := stub.GetState(id)

	if err != nil {
		return shim.Error("Failed to get network service:" + id)
	}
	if serviceAsBytes == nil {
		jsonResp := "{\"Error\":\"Service does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(serviceAsBytes)

}

func registerNService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	//Input sanitization
	if len(args) != 3 {
		return shim.Error("Incorect number of arguments. Excpecting 3")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument (Tenant Pck) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument (Network Service Id) must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument (Network Service name) must be a non-empty string")
	}

	tenantAsBytes, err := stub.GetState(args[0])

	//Check if tenant owner of service to update exists
	if err != nil {
		return shim.Error("Failed to get tenant:" + err.Error())
	}
	if tenantAsBytes == nil {
		jsonResp := "{\"Error\":\"Tenant does not exist: " + args[0] + "\"}"
		return shim.Error(jsonResp)
	}

	serviceTenant := new(Tenant)
	_ = json.Unmarshal(tenantAsBytes, serviceTenant)

	service := NetworkService{
		ObjectType: "network_service",
		ID:         args[1],
		Name:       args[2],
		Tenant:     *serviceTenant,
	}

	serviceAsBytes, _ := json.Marshal(service)
	err = stub.PutState(service.ID, serviceAsBytes)

	return shim.Success(serviceAsBytes)

}

//Function to unregister Service
func unregisterNService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	var jsonResp string

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Network Service Id)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Network Service Id) must be a non-empty string")
	}

	id := args[0]
	serviceAsBytes, err := stub.GetState(id)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + id + "\"}"
		return shim.Error(jsonResp)
	} else if serviceAsBytes == nil {
		jsonResp = "{\"Error\":\"Service does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}

	err = stub.DelState(id) //remove the flow from chaincode state
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}

	return shim.Success(serviceAsBytes)

}
