/*
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"encoding/json"
	//"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

//Function to read Service
func readService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Service Id)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Service Id) must be a non-empty string")
	}

	id := args[0]
	serviceAsBytes, err := stub.GetState(id)

	if err != nil {
		return shim.Error("Failed to get Service:" + id)
	}
	if serviceAsBytes == nil {
		jsonResp := "{\"Error\":\"Service does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(serviceAsBytes)

}

//Function to register Service
func registerService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	//Input sanitization
	if len(args) != 6 {
		return shim.Error("Incorect number of arguments. Excpecting 6")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument (Service Id) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument (Service Name) must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument (Service Short Name) must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument (Service Description) must be a non-empty string")
	}
	if len(args[4]) <= 0 {
		return shim.Error("5th argument (Network Descriptor Name) must be a non-empty string")
	}
	if len(args[5]) <= 0 {
		return shim.Error("6th argument (Service Provider) must be a non-empty string")
	}

	networkServiceAsBytes, err := stub.GetState(args[4])

	//Check if tenant owner of service to update exists
	if err != nil {
		return shim.Error("Failed to get provider (Network Service):" + err.Error())
	}
	if networkServiceAsBytes == nil {
		jsonResp := "{\"Error\":\"Network Service does not exist: " + args[4] + "\"}"
		return shim.Error(jsonResp)
	}

	service := Service{
		ObjectType:  "service",
		ID:          args[0],
		Name:        args[1],
		ShortName:   args[2],
		Description: args[3],
		NsdName:     args[4],
		Provider:    args[5],
	}

	serviceAsBytes, _ := json.Marshal(service)
	err = stub.PutState(service.ID, serviceAsBytes)

	return shim.Success(serviceAsBytes)

}

//Function to unregister Service
func unregisterService(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	var jsonResp string

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 (Service Id)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Service Id) must be a non-empty string")
	}

	id := args[0]
	serviceAsBytes, err := stub.GetState(id)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + id + "\"}"
		return shim.Error(jsonResp)
	} else if serviceAsBytes == nil {
		jsonResp = "{\"Error\":\"Service does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}

	err = stub.DelState(id)
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}

	return shim.Success(serviceAsBytes)

}
