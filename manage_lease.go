/*
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"encoding/json"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

func readLease(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2 (Vnf and Recipient)")
	}
	if len(args[0]) <= 0 {
		return shim.Error("(Service) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("(Recipient) must be a non-empty string")
	}

	leaseAsBytes, err := stub.GetState(args[0] + "," + args[1])

	if err != nil {
		return shim.Error("Failed to get lease:" + args[0] + "," + args[1])
	}
	if leaseAsBytes == nil {
		jsonResp := "{\"Error\":\"Lease does not exist: " + args[0] + "," + args[1] + "\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(leaseAsBytes)

}

func registerLease(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	//Input sanitization
	if len(args) != 6 {
		return shim.Error("Incorect number of arguments. Excpecting 6")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument (Grantor) must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument (Recipient) must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument (Service) must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument (Issue) must be a non-empty string")
	}
	if len(args[4]) <= 0 {
		return shim.Error("5th argument (Expiry) must be a non-empty string")
	}
	if len(args[5]) <= 0 {
		return shim.Error("6th argument (Revokers) must be a non-empty string")
	}
	//Converting integer arguments
	issue, err := strconv.ParseInt(args[3], 10, 64)
	expiry, err := strconv.ParseInt(args[4], 10, 64)
	//Converting json array to string array
	var revokers []string
	_ = json.Unmarshal([]byte(args[5]), &revokers)

	//Check if Lease exists
	params := []string{args[2], args[1]}
	leaseAsBytes := readLease(stub, params).Payload
	if leaseAsBytes != nil {
		jsonResp := "{\"Error\":\"Lease exists: " + params[0] + "," + params[1] + "\"}"
		return shim.Error(jsonResp)
	}

	//Check if Service exists
	serviceAsBytes, err := stub.GetState(args[2])
	if err != nil {
		return shim.Error("Failed to get Service:" + args[2])
	}
	if serviceAsBytes == nil {
		jsonResp := "{\"Error\":\"Service for lease does not exist: " + args[2] + "\"}"
		return shim.Error(jsonResp)
	}

	//Check if VNF belongs to Grantor
	service := new(Service)
	_ = json.Unmarshal(serviceAsBytes, service)

	if service.Provider != args[0] {
		return shim.Error("Service doesn't belong to Grantor:" + args[0])
	}

	//Register lease
	lease := Lease{
		ObjectType: "lease",
		Grantor:    args[0],
		Recipient:  args[1],
		Service:    args[2],
		Issue:      issue,
		Expiry:     expiry,
		Revokers:   revokers,
	}
	leaseAsBytes, _ = json.Marshal(lease)
	err = stub.PutState(lease.Service+","+lease.Recipient, leaseAsBytes)

	if err != nil {
		return shim.Error("Failed to register lease " + lease.Service + "," + lease.Recipient)
	}

	return shim.Success(leaseAsBytes)

}

func isLeaseValid(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	leaseAsBytes := readLease(stub, args).Payload
	lease := new(Lease)
	_ = json.Unmarshal(leaseAsBytes, lease)
	timenow := int64(time.Now().Unix())

	if lease.Expiry > timenow {
		return shim.Success([]byte("true"))
	}

	return shim.Success([]byte("false"))

}
